using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeakSpot : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            Destroy(transform.parent.gameObject);
            collision.gameObject.GetComponent<PlayerMovement>().Jump();
        }
    }
}
