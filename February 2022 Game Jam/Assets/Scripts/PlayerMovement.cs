using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    float speed = 5.0f;
    public float playerJumpHeight = 5;
    // public CharacterController CharacterController;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        // CharacterController = GetComponent<CharacterController>();
        
        Physics.gravity = new Vector3(0, -30.0F, 0);
    }

    void Update() {
        UpdateWithRigidbody();
    }

    // void UpdateWithCharacterController() {
    //     //Movement
    //     var horizontal = Input.GetAxisRaw("Horizontal");
    //     var vertical = Input.GetAxisRaw("Vertical");

    //     var direction = new Vector3(horizontal, 0, vertical).normalized;

    //     if (direction.magnitude >= 0.1f) {
    //         CharacterController.Move(direction * speed * Time.deltaTime);
    //     }
        
    //     //Gravity
    //     var moveVector = Vector3.zero;
    //     if (CharacterController.isGrounded == false)
    //     {
    //         moveVector += Physics.gravity;
    //     }
    //     CharacterController.Move(moveVector * Time.deltaTime);

    //     //Jump
    //     if(Input.GetKeyDown(KeyCode.Space) && CharacterController.isGrounded) {
    //         var jumpDirection = Vector3.zero;
    //         jumpDirection -= Physics.gravity*1000;
    //         CharacterController.Move(jumpDirection * Time.deltaTime);
    //     }
    // }

    Rigidbody rb; 
    float movementSpeed = 10f;
    float jumpForce = 15f;
    [SerializeField] Transform groundCheck;
    [SerializeField] LayerMask ground;

    void UpdateWithRigidbody() {
        //Movement
        var horizontal = Input.GetAxisRaw("Horizontal");
        var vertical = Input.GetAxisRaw("Vertical");

        rb.velocity = new Vector3(horizontal * movementSpeed, rb.velocity.y, vertical * movementSpeed);

        //Jump
        if(Input.GetKeyDown(KeyCode.Space) && IsGrounded())
        {
            Jump();
        }
    }

    public void Jump()
    {
        rb.velocity = new Vector3(rb.velocity.x, jumpForce, rb.velocity.z);
    }

    bool IsGrounded() {
        return Physics.CheckSphere(groundCheck.position, .1f, ground);
    }
}